using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace NetworkMonitor
{
    class Program
    {
        static bool isDown = false;
        static DateTime failureStart;
        static string fileName = "log.txt";
    
        static void Main(string[] args)
        {
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromSeconds(5);
            
            var timer = new System.Threading.Timer((e) => { CheckConnection(); }, null, startTimeSpan, periodTimeSpan);

            Console.Read();
        }

        private static void CheckConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    Console.WriteLine("Connected");

                  if (isDown)
                  {
                    Console.WriteLine("Failure lasted for {0}", DateTime.Now - failureStart);
                    WriteToErrorLog(failureStart, DateTime.Now);
                  }

                  isDown = false;
                }
            }
            catch
            {
                Console.WriteLine("Failure - {0}", DateTime.Now);
                if (!isDown)
                    failureStart = DateTime.Now;

                isDown = true;
            }
        }
      
        private static void WriteToErrorLog(DateTime start, DateTime end)
        {
          var fs = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.Write);
          var sw = new StreamWriter(fs);
          sw.WriteLine($"Failure at {start}, lasted for {end - start}");
          sw.WriteLine();
          sw.Close();
        }
    }
}
